To run this game, install the LOVE 2D game engine. Debian/Ubuntu, Fedora/Red Hat, and SUSE users should be able to find it in the package manager under the name 'love'. If it is not available in your distro's repositories, you can download the latest version from http://love2d.org/
If the game doesn't work, your system might have an older version of L�ve preinstalled. Try updating to the latest.

# Arch Dependencies for love07 (not specified by AUR package)

See [love2d website](https://love2d.org/wiki/Building_L%C3%96VE) for full
dependency list.

- libmng
- glu (replaces libgl)


## Manual Installation

Just run `make` in this repository's root directory.

See [love2d docs](https://love2d.org/wiki/Game_Distribution) for love details.


1. archive:

    ```bash
    cd not-tetris/ && \
    zip -9 -r NotTetris.love
    ```
2. run: `love07 NotTetris.love`


